import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Course, RatingResult} from '../typing/types';

@Injectable()
export class RatingsRestService {
  constructor(private http: HttpClient) {
  }

  getRates(id: string): Observable<RatingResult[]> {
    return this.http.get<RatingResult[]>(`api/students/rating/course/${id}`);
  }
}

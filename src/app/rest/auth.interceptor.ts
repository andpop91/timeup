import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { AuthService } from './auth.service';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService) {}
  
  intercept(request, next: HttpHandler) {
    const token = this.authService.getToken();
    if (token) {
      request = request.clone(this.getRequestParams(request, token));
    }
    return next.handle(request);
  }

  private getRequestParams(request, token: string) {
    return {
      setHeaders: {
        'Authorization': `Bearer ${token}`,
        ...this.getContentTypeHeader(request)
      }
    };
  }

  private getContentTypeHeader(request: Request) {
    if (!(request.body instanceof FormData)) {
      return {
        'Content-Type': 'application/json'
      };
    }
  }
}

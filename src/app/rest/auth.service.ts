import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { RestClientService } from './rest-client.service';
import { UserDto } from './user.service';
import { BehaviorSubject, Observable } from 'rxjs';

interface LoginCredential {
  email: string;
  password: string;
}

@Injectable()
export class AuthService {
  private endPoint = 'https://10.123.32.241:3005';
  private readonly tokenKey = 'token';
  private readonly currentUserKey = 'currentUser';
  public isLogged = false;
  private currentUserSubject: BehaviorSubject<UserDto>;
  public currentUser: Observable<UserDto>;

  constructor(
    private router: Router,
    private rest: RestClientService
  ) {
    this.isLogged = this.isTokenExist();
    this.currentUserSubject = new BehaviorSubject<UserDto>(this.getCurrentUser());
    this.currentUser = this.currentUserSubject.asObservable();
  }

  private isTokenExist(): boolean {
    return Boolean(this.getToken());
  }

  getToken(): string | undefined {
    return localStorage.getItem(this.tokenKey);
  }

  private setToken(token: string) {
    localStorage.setItem(this.tokenKey, token);
  }

  private clearToken() {
    localStorage.removeItem(this.tokenKey);
  }

  getCurrentUserValue(): UserDto {
    return this.currentUserSubject.value;
  }

  private getCurrentUser(): UserDto {
    return JSON.parse(localStorage.getItem(this.currentUserKey));
  }

  private setCurrentUser(user: UserDto) {
    localStorage.setItem(this.currentUserKey, JSON.stringify(user));
  }

  private clearCurrentUser() {
    localStorage.removeItem(this.currentUserKey);
  }

  login(credential: LoginCredential) {
    return this.rest.http.post<{ studentDetails: UserDto, accessToken: string }>(`/api/login`, credential)
      .pipe(res => this.rest.handleResponse(res, {
        400: 'Invalid email or password'
      }),
        map(res => {
          const { studentDetails, accessToken } = res;
          if (accessToken && studentDetails) {
            this.setToken(accessToken);
            this.setCurrentUser(studentDetails);
            this.currentUserSubject.next(studentDetails);
            this.isLogged = true;
          }
        }));
  }

  logout() {
    this.clearToken();
    this.isLogged = false;
    this.clearCurrentUser();
    this.router.navigate(['']);
  }
}

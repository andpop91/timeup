import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Course} from '../typing/types';

@Injectable()
export class CoursesRestService {

  constructor(private http: HttpClient) {
  }

  getAllCourses(id: number): Observable<Course[]> {
    return this.http.get<Course[]>(`api/students/${id}/courses`);
  }
}

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RestClientService } from './rest-client.service';

export interface UserDto {
  id: number;
  name: string;
  email: string;
  password: string;
  passwordConfirmation: string;
  creditCardNumber: string;
  bornDate: Date;
  phoneNumber: string;
  position: string;
  framework: string;
}

@Injectable()
export class UserService {
  public endPoint = 'http://localhost:3005/';

  constructor(private rest: RestClientService) {}

  getUsers(): Observable<UserDto[]> {
    return this.rest.http.get<UserDto[]>(`${this.endPoint}api/users`);
  }

  getUser(id: number): Observable<UserDto> {
    return this.rest.http.get<UserDto>(`${this.endPoint}api/users/${id}`);
  }

  updateUser(user: UserDto) {
    const { id } = user;
    return this.rest.http.put<UserDto>(`${this.endPoint}api/users/${id}`, user)
      .pipe(res => this.rest.handleResponse(res, {
          200: 'User was successfully updated'
        })
      );
  }
}

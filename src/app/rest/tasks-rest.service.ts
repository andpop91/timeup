import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Task} from '../typing/types';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class TasksRestService {

  constructor(private http: HttpClient) {
  }

  getAllTasks(studentId: number, courseId: string): Observable<Task[]> {
    return this.http.get<Task[]>(`api/students/${studentId}/courses/${courseId}/tasks`);
  }

  updateTask(taskId: string, newData): Observable<Task[]> {
    return this.http.put<Task[]>(`api/tasks/${taskId}`, newData);
  }
}

// api/students/:studentId/courses/:courseId/tasks

import {Subject} from 'rxjs';

interface Course {
  id: number;
  name: string;
}

interface Task {
  id: string;
  name: string;
  description: string;
  status: string;
  estimatedTime: number;
  timeStart: Date;
  timeOfWork: null | number;
  deadLine: Date;
  createdAt: Date;
  updatedAt: Date;
  courseId: number;
  studentId: number;
  collapse?: boolean;
  count?: number;
  activeTimer?: boolean;
  isClosed?: boolean;
}

interface PopoverTimer {
  id: string;
  name: string;
  count: number;
  timer: Subject<number>;
  activeTimer?: boolean;
  courseId?: string;
}

interface ActiveTask {
  id: string;
  timer: Subject<number>;
}

interface TaskUpdate {
  status: string;
  timeOfWork?: number;
}

interface RatingResult {
  id: string;
  email: string;
  firstName: string;
  lastName: string;
  gitLabToken: string;
  createdAt: Date;
  updatedAt: Date;
  Tasks: [
    {
      timeOfWork: null
    }
  ];
}

export {Course, Task, PopoverTimer, ActiveTask, TaskUpdate, RatingResult};


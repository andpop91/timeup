import {Component, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.scss']
})
export class TimerComponent {

  @Input() 
  seconds: number;
  @Input()
  showButtons: boolean;
  @Output()
  start = new EventEmitter<string>();
  @Output()
  pause = new EventEmitter<string>();
  @Output()
  stop = new EventEmitter<string>();

  startCount(event) {
    event.stopPropagation();
    this.start.emit('');
  }

  pauseCount(event) {
    event.stopPropagation();
    this.pause.emit('');
  }

  stopCount(event) {
    event.stopPropagation();
    this.stop.emit('');
  }

}

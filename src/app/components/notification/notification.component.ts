import { Component, OnInit, OnDestroy } from '@angular/core';
import { Notification, NotificationService } from '../../modules/services/notification.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html'
})
export class NotificationComponent implements OnInit, OnDestroy {
  private message: Notification;
  private subscription: Subscription;

  constructor(private notificationService: NotificationService) {}

  ngOnInit() {
    this.subscription = this.notificationService.getMessage()
      .subscribe(message => this.message = message);
  }

  clearMessage(message: Notification) {
    this.notificationService.clearMessage();
  }

  private getClassByNotificationType(type: string): string {
    const classes = {
      success: 'bg-success',
      error: 'bg-danger text-white',
      info: 'bg-info',
      warning: 'bg-warning'
    };

    return classes[type] || 'bg-white';
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}

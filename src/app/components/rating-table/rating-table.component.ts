import {Component, Input, OnInit} from '@angular/core';
import {RatingResult} from '../../typing/types';

@Component({
  selector: 'app-rating-table',
  templateUrl: './rating-table.component.html',
  styleUrls: ['./rating-table.component.scss']
})
export class RatingTableComponent {

  @Input() rateList: RatingResult [];

  constructor() {
  }
}

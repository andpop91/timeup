import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { TimerModule } from '../timer/timer.module';

@NgModule({
  declarations: [ HeaderComponent ],
    imports: [
      CommonModule,
      TimerModule
    ],
  exports: [ HeaderComponent ]
})
export class LayoutModule {}

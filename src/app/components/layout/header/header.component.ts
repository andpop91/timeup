import { Component, EventEmitter, Input, Output } from '@angular/core';
import { TimersControlService } from 'src/app/modules/services/timers-control.service';
import { PopoverTimer, ActiveTask } from 'src/app/typing/types';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  private openTasks = 0;
  private tasks: PopoverTimer[] = [];
  @Input() companyName: string;
  @Input() userName: string;

  @Output() logoutEvent = new EventEmitter<boolean>();

  constructor(
    private timersControl: TimersControlService,
    private router: Router) {}

  ngOnInit() {
    this.timersControl
      .getActiveTasks()
      .subscribe(newTask => {
        let currTask = this.tasks.find(item => item.id === newTask.id);
        if (!currTask) {
          this.tasks.push(newTask);
          newTask.timer.subscribe(count => {
            newTask.count = count;
          });
        } else {
          currTask.timer = newTask.timer;
          currTask.timer.subscribe(count => {
            currTask.count = count;
          });
        }
        this.openTasks = this.tasks.length;
      });
  }

  isEmptyList() {
    return this.tasks.length > 0;
  }

  startTimer(taskId: string) {
    let task = this.tasks.find(item => item.id === taskId);
    if (task.activeTimer) {
      return;
    } 
    let timer: Subject<number> = this.timersControl.createTimer();
    timer = this.timersControl.startCount(timer, task.count || 0);
    timer.subscribe(val => task.count = val);
    task.activeTimer = true;
    let newActiveTask: PopoverTimer = {
      id: taskId,
      name: task.name,
      timer,
      count: task.count || 0
    };
    this.timersControl.addActiveTask(newActiveTask);
  }

  pauseTimer(taskId: string) {
    let timer: Subject<number>;
    timer = this.timersControl.getTimer(taskId);
    timer.complete();
    this.timersControl.removeTimer(taskId);
    let task = this.tasks.find(item => item.id === taskId);
    task.activeTimer = false; 
    this.timersControl.setTaskDeactivate(task.id);
  }

  navigateToTasks(courseId: string) {
    this.router.navigateByUrl(`/portal/main/courses/${courseId}/tasks`);
  }

  logout() {
    this.logoutEvent.emit(false);
  }
}

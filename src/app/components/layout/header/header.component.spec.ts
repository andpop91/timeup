import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HeaderComponent } from './header.component';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Should render company name', () => {
    component.companyName = 'SPD Ukraine';
    fixture.detectChanges();
    const element: HTMLElement = fixture.nativeElement;
    const h5 = element.querySelector('h5');
    expect(h5.textContent.trim()).toEqual('SPD Ukraine');
  });
});

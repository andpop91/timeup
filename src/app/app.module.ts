import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AppRouting } from './app.routing';
import { AuthModule } from './modules/guards/auth/auth.module';
import { AuthService } from './rest/auth.service';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AuthInterceptor } from './rest/auth.interceptor';
import { NotificationService } from './modules/services/notification.service';
import { LoginModule } from './containers/login/login.module';
import { NotificationComponent } from './components/notification/notification.component';
import { RestClientService } from './rest/rest-client.service';
import { CoursesRestService } from './rest/courses-rest.service';
import {TasksRestService} from './rest/tasks-rest.service';
import {TimersControlService} from './modules/services/timers-control.service';
import {RatingsRestService} from './rest/rating-rest.service';

@NgModule({
  declarations: [
    AppComponent,
    NotificationComponent,
  ],
  imports: [
    BrowserModule,
    AppRouting,
    AuthModule,
    HttpClientModule,
    LoginModule,
  ],
  bootstrap: [AppComponent],
  providers: [
    AuthService,
    RatingsRestService,
    NotificationService,
    CoursesRestService,
    TasksRestService,
    TimersControlService,
    RestClientService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
  ]
})
export class AppModule {}

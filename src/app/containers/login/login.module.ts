import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { LoginRouting } from './login.routing';
import { FormsModule } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';

@NgModule({
  declarations: [LoginComponent],
  imports: [
    CommonModule,
    LoginRouting,
    FormsModule,
    CustomFormsModule
  ],
  exports: [LoginComponent]
})
export class LoginModule {}

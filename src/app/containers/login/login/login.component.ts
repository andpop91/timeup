import { Component } from '@angular/core';
import { AuthService } from '../../../rest/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  public credential = {
    email: '',
    password: ''
  };

  constructor(
    private authService: AuthService,
    private router: Router
  ) {}

  login() {
    this.authService.login(this.credential)
      .subscribe(() => {
        if (this.authService.isLogged) {
          this.router.navigate(['portal']);
        }
      });
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PortalComponent } from './portal/portal.component';
import { LayoutModule } from '../../components/layout/layout.module';
import { PortalRouting } from './portal.routing';
import { UserService } from '../../rest/user.service';

@NgModule({
  declarations: [ PortalComponent ],
  imports: [
    CommonModule,
    LayoutModule,
    PortalRouting
  ],
  exports: [ PortalComponent ],
  providers: [UserService]
})
export class PortalModule {}

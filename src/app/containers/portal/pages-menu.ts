import { MenuInfo } from './portal/portal.component';

export const MAIN_MENU: MenuInfo[] = [
  {
    title: 'Users',
    route: 'users',
    icon: 'people'
  },
  {
    title: 'Settings',
    route: 'settings',
    icon: 'settings_applications'

  },
  {
    title: 'Reports',
    route: 'reports',
    icon: 'timeline'
  },
];

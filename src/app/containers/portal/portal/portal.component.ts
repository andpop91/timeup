import { Component, OnInit } from '@angular/core';
import { MAIN_MENU } from '../pages-menu';
import { Router } from '@angular/router';
import { AuthService } from '../../../rest/auth.service';
import { UserDto } from '../../../rest/user.service';

export interface MenuInfo {
  title: string;
  route: string;
  icon?: string;
}

@Component({
  selector: 'app-portal',
  templateUrl: './portal.component.html',
  styleUrls: ['./portal.component.scss']
})
export class PortalComponent implements OnInit {
  mainMenu: MenuInfo[] = [];
  companyName = 'SPD University';
  user: UserDto;

  constructor(
    private router: Router,
    private authService: AuthService
  ) {}

   ngOnInit() {
     this.mainMenu = MAIN_MENU;
     this.user = this.authService.getCurrentUserValue();
   }

  logout(logged: boolean) {
    if (!logged) {
      this.authService.logout();
    }
  }
}

import {Component, OnChanges, OnInit} from '@angular/core';
import {AuthService} from '../../rest/auth.service';
import {TasksRestService} from '../../rest/tasks-rest.service';
import {ActivatedRoute} from '@angular/router';
import {Task, ActiveTask, PopoverTimer, TaskUpdate, RatingResult} from '../../typing/types';
import {TimersControlService} from 'src/app/modules/services/timers-control.service';
import {Subject} from 'rxjs';
import {RatingsRestService} from '../../rest/rating-rest.service';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss']
})
export class TasksComponent implements OnInit {
  tasksList: Task[] = [];
  activeTasks: ActiveTask[] = [];
  courseId: string;
  ratingList: RatingResult [];

  constructor(
    private authService: AuthService,
    private tasksService: TasksRestService,
    private activatedRoute: ActivatedRoute,
    private timersControl: TimersControlService,
    private ratingService: RatingsRestService
  ) {
  }

  toggle(index: number) {
    this.tasksList[index].collapse = !this.tasksList[index].collapse;
  }

  ngOnInit() {
    const userId = this.authService.getCurrentUserValue().id;
    this.courseId = this.activatedRoute.snapshot.params.courseId;
    this.tasksService
      .getAllTasks(userId, this.courseId)
      .subscribe(
        response => {
          this.tasksList = response.map(task => {
            return {
              ...task,
              timeOfWork: task.timeOfWork === null ? 0 : task.timeOfWork,
              count: task.timeOfWork === null ? 0 : task.timeOfWork,
              isClosed: task.status === 'closed',
              collapse: false,
              activeTimer: false
            };
          });
        }
      );

    this.ratingService
      .getRates(this.courseId)
      .subscribe(
        response => {
          this.ratingList = response;
        }
      );

    this.timersControl
      .getActiveTasks()
      .subscribe(newTask => {
        const currTask = this.tasksList.find(item => item.id === newTask.id);
        currTask.activeTimer = false;
        let activeTask: ActiveTask = this.activeTasks.find(item => item.id === newTask.id);
        if (!activeTask) {
          activeTask = {
            id: newTask.id,
            timer: newTask.timer
          };
          this.activeTasks.push(activeTask);
        }
        activeTask.timer = newTask.timer;
        activeTask.timer.subscribe(count => {
          currTask.count = count;
        });
      });
    this.timersControl
      .getDeactivateTasks()
      .subscribe(taskId => {
        const currTask = this.tasksList.find(item => item.id === taskId);
        currTask.activeTimer = false;
      });
  }

  startTimer(taskId: string) {
    let timer: Subject<number>;
    const task = this.tasksList.find(item => item.id === taskId);
    if (task.activeTimer) {
      return;
    }
    const activeTask: ActiveTask = this.activeTasks.find(item => item.id === taskId);
    if (!activeTask) {
      timer = this.timersControl.createTimer();
      this.timersControl.addTimer(taskId, timer);
      const newActiveTask: ActiveTask = {
        id: taskId,
        timer
      };
      this.activeTasks.push(newActiveTask);
    } else {
      timer = activeTask.timer;
    }
    timer = this.timersControl.startCount(timer, task.count || 0);
    const newActiveTask2: PopoverTimer = {
      id: taskId,
      name: task.name,
      timer,
      count: task.count || 0
    };
    this.timersControl.addActiveTask(newActiveTask2);
    timer.subscribe(val => task.count = val);
    task.activeTimer = true;
    const newData: TaskUpdate = {
      status: 'inWork',
    };
    if (task.timeOfWork === 0) {
      newData.timeOfWork = 0;
    }
    this.tasksService
      .updateTask(task.id, newData)
      .subscribe();
  }

  pauseTimer(taskId: string) {
    let timer: Subject<number>;
    timer = this.timersControl.getTimer(taskId);
    timer.complete();
    this.timersControl.removeTimer(taskId);
    this.activeTasks = this.activeTasks.filter(item => item.id !== taskId);
    const task = this.tasksList.find(item => item.id === taskId);
    task.activeTimer = false;
    const timeOfWork: number = task.timeOfWork !== 0 ? task.count : 0;
    const newData = {
      status: 'paused',
      timeOfWork
    };
    task.timeOfWork = timeOfWork;
    task.count = timeOfWork;
    this.tasksService
      .updateTask(taskId, newData)
      .subscribe();
  }

  stopTimer(taskId: string) {
    let timer: Subject<number>;
    timer = this.timersControl.getTimer(taskId);
    if (timer) {
      timer.complete();
    }
    this.timersControl.removeTimer(taskId);
    this.activeTasks = this.activeTasks.filter(item => item.id !== taskId);
    const task = this.tasksList.find(item => item.id === taskId);
    if (task.hasOwnProperty('activeTimer')) {
      task.activeTimer = false;
    }
    task.isClosed = true;
    const timeOfWork: number = task.timeOfWork !== 0 ? task.count : 0;
    const newData = {
      status: 'closed',
      timeOfWork
    };
    task.timeOfWork = timeOfWork;
    task.count = timeOfWork;
    this.tasksService
      .updateTask(taskId, newData)
      .subscribe();
  }
}

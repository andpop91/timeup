import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MainPageComponent} from './main-page.component';
import {MainPageRouting} from './main-page.routing';
import {CoursesComponent} from './courses/courses.component';
import {TasksComponent} from '../tasks/tasks.component';
import {RatingTableComponent} from '../../components/rating-table/rating-table.component';
import { TimerModule } from 'src/app/components/timer/timer.module';

@NgModule({
  declarations: [
    MainPageComponent,
    CoursesComponent,
    TasksComponent,
    RatingTableComponent,
  ],
  imports: [
    CommonModule,
    TimerModule,
    MainPageRouting,
  ],
  exports: [
    MainPageComponent,
    CoursesComponent,
    TasksComponent,
    RatingTableComponent,
  ]
})
export class MainPageModule {
}

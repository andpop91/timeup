import {Component, OnInit} from '@angular/core';
import {CoursesRestService} from 'src/app/rest/courses-rest.service';
import {Course} from 'src/app/typing/types';
import {UserDto} from 'src/app/rest/user.service';
import {AuthService} from 'src/app/rest/auth.service';
import {Router} from '@angular/router';
import {TasksRestService} from '../../../rest/tasks-rest.service';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.scss']
})
export class CoursesComponent implements OnInit {
  private coursesList: Course[] = [];
  private currentUser: UserDto;
  private closedTasks: Array<number> = [];
  private theWholetasksNumber: Array<number> = [];

  constructor(
    private coursesService: CoursesRestService,
    private authService: AuthService,
    private tasksService: TasksRestService,
    private router: Router
  ) { }

  ngOnInit() {
    this.currentUser = this.authService.getCurrentUserValue();
    this.coursesService
      .getAllCourses(this.currentUser.id)
      .subscribe(
        response => {
          this.coursesList = response;
          this.coursesList.map((_, i) => {
            this.tasksService
              .getAllTasks(this.currentUser.id, `${this.coursesList[i].id}`)
              .subscribe(
                responseNew => {
                  this.theWholetasksNumber[i] = responseNew.length;
                  this.closedTasks[i] = 0;
                  for (const key of responseNew) {
                    if (key.status === 'closed') {
                      this.closedTasks[i] += 1;
                    }
                  }
                }
              );
          });
        },
        error => {
          throw error;
        }
      );
  }


  navigateToTasks(courseId: number) {
    this.router.navigateByUrl(`/portal/main/courses/${courseId}/tasks`);
  }
}

import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {MainPageComponent} from './main-page.component';
import {CoursesComponent} from './courses/courses.component';
import {TasksComponent} from '../tasks/tasks.component';

const routes: Routes = [
  {
    path: '',
    component: MainPageComponent,
    children: [
      {
        path: '',
        redirectTo: 'courses'
      },
      {
        path: 'courses',
        component: CoursesComponent
      },
      {
        path: 'courses/:courseId/tasks',
        component: TasksComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainPageRouting {
}

import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { AuthService } from '../../../rest/auth.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private  router: Router,
    private authService: AuthService
  ) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (!this.authService.isLogged) {
      this.router.navigate(['']);
      return false;
    }

    return true;
  }
}

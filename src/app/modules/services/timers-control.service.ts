import { Injectable } from '@angular/core';
import { TimerService } from './timer-service';
import { Subject } from 'rxjs';
import { PopoverTimer } from 'src/app/typing/types';

@Injectable()
export class TimersControlService {
  private counters: Map<String, Subject<number>> = new Map();
  private activeTasksObs: Subject<PopoverTimer>;
  private deactivateTask: Subject<string>;
  private activeTasks: Partial<PopoverTimer>[] = [];

  constructor() {
    this.activeTasksObs = new Subject();
    this.deactivateTask = new Subject();
  }

  getActiveTasks(): Subject<PopoverTimer> {
    return this.activeTasksObs;
  }

  getDeactivateTasks(): Subject<string> {
    return this.deactivateTask;
  }

  activeTaskExist(taskId: string): boolean {
    let activeTask = this.activeTasks.find(item => item.id === taskId);  
    if (activeTask) {
      return true;
    } else {
      return false;
    }
  }

  addActiveTask(task: PopoverTimer) {
    this.counters.set(task.id, task.timer);
    this.activeTasks.push(task);
    const newTask: PopoverTimer = {
      id: task.id,
      name: task.name,
      timer: task.timer,
      count: 0
    };
    this.activeTasksObs.next(newTask);
  }

  setTaskDeactivate(taskId: string) {
    this.deactivateTask.next(taskId);
  }

  addTimer(taskId: string, timer: Subject<number>) {
    this.counters.set(taskId, timer);
    let activeTask = this.activeTasks.find(item => item.id === taskId);
    if (activeTask) {
      activeTask.timer = timer;
    }
  }

  createTimer(): Subject<number> {
    return new Subject();
  }

  getTimer(taskId: string): Subject<number> {
    const timer: Subject<number> = this.counters.get(taskId);
    return timer;
  }

  removeTimer(taskId: string) {
    this.counters.delete(taskId);
  }

  startCount(timer: Subject<number>, initValue: number): Subject<number> {
    setInterval(() => {
      timer.next(initValue++);
    }, 1000);
    return timer;
  }

}
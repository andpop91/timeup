import { Subject } from 'rxjs';

export class TimerService {
  private timer: Subject<number>;
  private count: number = 0;

  constructor() {
    this.timer = new Subject();
  }
  getTimer(): Subject<number> {
    return this.timer;
  }

  startCount(timer: Subject<number>): Subject<number> {
    setInterval(() => {
      timer.next(this.count++);
    }, 1000);
    return timer;
  }
}